from django.shortcuts import render

# Create your views here.

def index(request):
    response = {}
    response['author'] = "Anan"
    return render(request, 'lab_8/lab_8.html', response)