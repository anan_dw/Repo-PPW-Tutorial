from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
import json

# Create your tests here.
class Lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
		
	def test_lab7_add_friend_success(self):
		nama = 'Anan'
		npm = '1606917720'
		data = {'name': nama, 'npm': npm}
		response_post = Client().post('/lab-7/add-friend/', data)
		self.assertEqual(response_post.status_code, 200)

	def test_npm_valid(self):
		new_friend = Friend.objects.create(friend_name='Anan', npm='1606917720')
		self.assertFalse(npmIsValid('1606917720'))
		self.assertTrue(npmIsValid('123456789'))

	def test_validate_npm(self):
		new_friend = Friend.objects.create(friend_name='Anan', npm='1606917720')
		response = Client().post('/lab-7/validate-npm/', {'npm': new_friend.npm})
		self.assertEqual(dict, type(response.json()))

	def test_lab_7_friend_list_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_friend_list_using_friend_list_func(self):
		response = resolve('/lab-7/get-friend-list/')
		self.assertEqual(response.func, friend_list)

	def test_lab_7_friend_list_using_daftar_func(self):
		response = resolve('/lab-7/daftar/')
		self.assertEqual(response.func, daftar)

	def test_lab_7_can_list_friends(self):
		Friend.objects.create(friend_name='test', npm='123')
		response = Client().post('/lab-7/daftar/')
		respDict = json.loads(response.content.decode('utf-8'))
		self.assertIn('test',respDict['list'][0]['friend_name'])

	def test_lab_7_can_validate_existing_npm(self):
		Friend.objects.create(friend_name='test', npm='123')
		friend_obj = Friend.objects.all()[0]
		response = Client().post('/lab-7/validate-npm/', {'npm':friend_obj.npm})
		respDict = json.loads(response.content.decode('utf-8'))
		self.assertTrue(respDict['is_taken'])

	def test_csui_helper_with_wrong_password(self):
		try:
			csui_helper = CSUIhelper('anonim', 'test')
			
		except Exception as e:
			self.assertIn('username atau password sso salah', str(e))

	def test_lab_7_can_delete_object(self):
		Friend.objects.create(friend_name='test', npm='123')
		friend_obj = Friend.objects.all()[0]
		response = Client().post('/lab-7/delete-friend/', {'id':friend_obj.id})
		self.assertEqual(Friend.objects.all().count(), 0)


	# def test_csui_helper_functions(self):
	# 	csui_helper = CSUIhelper();
	# 	client_id = csui_helper.get_client_id()
	# 	dic = csui_helper.get_auth_param_dict
	# 	self.assertEqual(client_id,'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
	# 	self.assertIn('X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG',dic[client_id])

		