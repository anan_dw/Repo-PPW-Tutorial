from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger    
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict
from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
response['author'] = "Anan"
csui_helper = CSUIhelper()


def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    list_to_objects(mahasiswa_list)
    paginator = Paginator(mahasiswa_list, 10) # Show 10 mahasiswa per page
    page = request.GET.get('page')
    try:
        mahasiswa_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswa_list = paginator.page(1)
    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    response['author'] = "Anan"
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def list_to_objects(lst):
    for data in lst:
        nama = data['nama']
        npm = data['npm']
        Mahasiswa.objects.get_or_create(mahasiswa_nama=nama, npm=npm)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response['author'] = "Anan"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        valid = npmIsValid(npm)
        data = {"is_valid":valid}

        if (data["is_valid"]):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data['friend'] = model_to_dict(friend)
           
        return JsonResponse(data)

@csrf_exempt    
def delete_friend(request):
    if request.method == "POST":
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)


def npmIsValid(npm):
    res = True
    friend_list = Friend.objects.all()
    for friend in friend_list:
        if friend.npm == npm:
            res = False
            break
    return res


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)

    data = {
        'is_taken': not npmIsValid(npm)#lakukan pengecekan apakah Friend dgn npm tsb sudah ada
        }
    return JsonResponse(data)


def daftar(request):
    friends = Friend.objects.all()
    lst = []
    for friend in friends:
        lst.append(model_to_dict(friend))
    daftar = {'list' : lst}
    return JsonResponse(daftar)
